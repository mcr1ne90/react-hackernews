import React from 'react';
import axios from 'axios';
import P from 'bluebird';
import Infinite from 'react-infinite';

class ShowList extends React.Component {
    constructor() {
        super();
        this.state = {content: [], isInfiniteLoading: false, showStories: []};
    }

    buildElements(start, end) {
        let show = [];
        let showInfo = [];
        for (let i of this.state.showStories.slice(start, end)) {
            show.push(axios.get('https://hacker-news.firebaseio.com/v0/item/' + i + '.json'));
        }
        return P.all(show)
            .then(show => {
                for (let item of show) {
                    showInfo.push(<a href={item.data.url}>{item.data.title}</a>);
                }
                return showInfo.map(entry =>
                    <div className="List" key={(showInfo.indexOf(entry) + start).toString()}>
                        <li className="Entry">
                            {entry}
                        </li>
                    </div>
                );
            });
    }

    componentDidMount() {
        let show = [];
        return axios.get('https://hacker-news.firebaseio.com/v0/showstories.json')
            .then(ids => {
                for (let i of ids.data) {
                    show.push(i);
                }
                this.setState({showStories: show});
            });
    }

    handleInfiniteLoad = () => {
        this.setState({
            isInfiniteLoading: true
        });
        setTimeout(() => {
            let elemLength = this.state.content.length;
            this.buildElements(elemLength, elemLength + 20)
                .then(result => this.setState({
                    isInfiniteLoading: false,
                    content: this.state.content.concat(result)
                }));
        }, 500);
    };

    elementInfiniteLoad() {
        return <div>

        </div>;
    }

    render() {
        return (
            <Infinite elementHeight={45}
                      useWindowAsScrollContainer
                      infiniteLoadBeginEdgeOffset={200}
                      onInfiniteLoad={this.handleInfiniteLoad}
                      loadingSpinnerDelegate={this.elementInfiniteLoad()}
                      isInfiniteLoading={this.state.isInfiniteLoading}
            >
                {this.state.content}
            </Infinite>
        );
    }
}

export default ShowList