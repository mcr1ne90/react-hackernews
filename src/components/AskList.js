import React from 'react';
import axios from 'axios';
import P from 'bluebird';
import CommentsList from './CommentsList';
import Infinite from 'react-infinite';

class AskList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {content: [], askStories: [], isInfiniteLoading: false};
    }

    componentWillMount() {
        let ask = [];
        return axios.get('https://hacker-news.firebaseio.com/v0/askstories.json')
            .then(ids => {
                for (let i of ids.data) {
                    ask.push(i);
                }
                this.setState({askStories: ask});
            })
    }

    buildElements(start, end) {
        let ask = [];
        let askInfo = [];
        let comments = [];
        for (let i of this.state.askStories.slice(start, end)) {
            ask.push(axios.get('https://hacker-news.firebaseio.com/v0/item/' + i + '.json'));
        }
        return P.all(ask)
            .then(ask => {
                for (let item of ask) {
                    askInfo.push(<a href={item.data.url}>{item.data.title}</a>);
                    if (item.data.kids) {
                        comments.push(item.data.kids);
                    }
                    else {
                        comments.push('There are no comments in this post');
                    }
                }
                return askInfo.map(entry =>
                    <div className="List" key={(askInfo.indexOf(entry) + start).toString()}>
                        <div className="Entry">
                            <li>
                                {entry}
                            </li>
                            <CommentsList data={comments[askInfo.indexOf(entry)]}/>
                        </div>
                    </div>
                );
            })
    }

    handleInfiniteLoad = () => {
        this.setState({
            isInfiniteLoading: true
        });
        setTimeout(() => {
            let elemLength = this.state.content.length;
            this.buildElements(elemLength, elemLength + 20)
                .then(result => this.setState({
                    isInfiniteLoading: false,
                    content: this.state.content.concat(result)
                }));
        }, 500);
    };

    elementInfiniteLoad() {
        return <div>

        </div>
    }

    render() {
        return (
            <Infinite elementHeight={70}
                      useWindowAsScrollContainer
                      infiniteLoadBeginEdgeOffset={200}
                      onInfiniteLoad={this.handleInfiniteLoad}
                      loadingSpinnerDelegate={this.elementInfiniteLoad()}
                      isInfiniteLoading={this.state.isInfiniteLoading}
            >
                {this.state.content}
            </Infinite>
        );
    }
}

export default AskList