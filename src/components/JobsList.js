import React from 'react';
import axios from 'axios';
import P from 'bluebird';

class JobsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {content: []};
    }

    componentDidMount() {
        let jobs = [];
        let jobsInfo = [];
        return axios.get('https://hacker-news.firebaseio.com/v0/jobstories.json')
            .then(ids => {
                for (let id of ids.data) {
                    jobs.push(axios.get('https://hacker-news.firebaseio.com/v0/item/' + id + '.json'))
                }
                return P.all(jobs);
            })
            .then(jobs => {
                for (let item of jobs) {
                    jobsInfo.push(<a href={item.data.url}>{item.data.title}</a>);
                }
                this.setState({content: jobsInfo})
            });
    }

    render() {
        const entries = this.state.content;
        const jobsEntries = entries.map(entry =>
            <li className="Entry" key={entries.indexOf(entry).toString()}>
                {entry}
            </li>
        );
        return (
            <div className="List">{jobsEntries}</div>);
    }
}

export default JobsList