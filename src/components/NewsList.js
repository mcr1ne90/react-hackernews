import React from 'react';
import axios from 'axios';
import P from 'bluebird';
import Infinite from 'react-infinite';

class NewsList extends React.Component {
    constructor() {
        super();
        this.state = {content: [], isInfiniteLoading: false, newStories: []};
    }

    buildElements(start, end) {
        let news = [];
        let newsInfo = [];
        for (let i of this.state.newStories.slice(start, end)) {
            news.push(axios.get('https://hacker-news.firebaseio.com/v0/item/' + i + '.json'));
        }
        return P.all(news)
            .then(news => {
                for (let item of news) {
                    newsInfo.push(<a href={item.data.url}>{item.data.title}</a>);
                }
                return newsInfo.map(entry =>
                    <div className="List" key={(newsInfo.indexOf(entry) + start).toString()}>
                        <li className="Entry">
                            {entry}
                        </li>
                    </div>
                );
            });
    }

    componentDidMount() {
        let news = [];
        return axios.get('https://hacker-news.firebaseio.com/v0/newstories.json')
            .then(ids => {
                for (let i of ids.data) {
                    news.push(i);
                }
                this.setState({newStories: news});
            });
    }

    handleInfiniteLoad = () => {
        this.setState({
            isInfiniteLoading: true
        });
        setTimeout(() => {
            let elemLength = this.state.content.length;
            this.buildElements(elemLength, elemLength + 20)
                .then(result => this.setState({
                    isInfiniteLoading: false,
                    content: this.state.content.concat(result)
                }));
        }, 500);
    };

    elementInfiniteLoad() {
        return <div>

        </div>;
    }

    render() {
        return (
            <Infinite elementHeight={45}
                      useWindowAsScrollContainer
                      infiniteLoadBeginEdgeOffset={200}
                      onInfiniteLoad={this.handleInfiniteLoad}
                      loadingSpinnerDelegate={this.elementInfiniteLoad()}
                      isInfiniteLoading={this.state.isInfiniteLoading}
            >
                {this.state.content}
            </Infinite>
        );
    }
}

export default NewsList