import React, { Component } from 'react';
import { Panel } from 'react-bootstrap';

class Profile extends Component {
    componentWillMount() {
        this.setState({ profile: {} });
        const { userProfile, getProfile } = this.props.auth;
        if (!userProfile) {
            getProfile((err, profile) => {
                this.setState({ profile });
            });
        } else {
            this.setState({ profile: userProfile });
        }
    }
    render() {
        const { profile } = this.state;
        return (
            <div>
                <div className="profile-area">
                    <Panel header="Profile">
                        <img src={profile.picture} alt="profile" />
                    </Panel>
                </div>
            </div>
        );
    }
}

export default Profile;