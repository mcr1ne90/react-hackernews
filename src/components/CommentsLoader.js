import React from 'react';

class CommentsLoader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {content: []};
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        this.setState({content: this.props.content.slice(0, 5)});
    }

    handleClick() {
        if (this.state.content.length < this.props.content.length) {
            this.setState({
                content: this.props.content.slice(0, this.state.content.length + 5)
            });
        }
    }

    render() {
        return (
            <div className="Comments">
                {this.state.content}
                {this.state.content.length < this.props.content.length ? <button onClick={this.handleClick}>Load more</button> : ''}
            </div>
        );
    }
}

export default CommentsLoader