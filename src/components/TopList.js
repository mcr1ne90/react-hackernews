import React from 'react';
import axios from 'axios';
import P from 'bluebird';
import CommentsList from './CommentsList';
import Infinite from 'react-infinite';

class TopList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {content: [], topStories: [], isInfiniteLoading: false};
    }

    componentWillMount() {
        let top = [];
        return axios.get('https://hacker-news.firebaseio.com/v0/topstories.json')
            .then(ids => {
                for (let i of ids.data) {
                    top.push(i);
                }
                this.setState({topStories: top});
            })
    }

    buildElements(start, end) {
        let top = [];
        let topInfo = [];
        let comments = [];
        for (let i of this.state.topStories.slice(start, end)) {
            top.push(axios.get('https://hacker-news.firebaseio.com/v0/item/' + i + '.json'));
        }
        return P.all(top)
            .then(top => {
                for (let item of top) {
                    topInfo.push(<a href={item.data.url}>{item.data.title}</a>);
                    if (item.data.kids)
                        comments.push(item.data.kids);
                    else {
                        comments.push('There are no comments in this post');
                    }
                }
                return topInfo.map(entry =>
                    <div className="List" key={(topInfo.indexOf(entry) + start).toString()}>
                        <div className="Entry">
                            <li>
                                {entry}
                            </li>
                            <CommentsList data={comments[topInfo.indexOf(entry)]}/>
                        </div>
                    </div>
                );
            })
    }

    handleInfiniteLoad = () => {
        this.setState({
            isInfiniteLoading: true
        });
        setTimeout(() => {
            let elemLength = this.state.content.length;
            this.buildElements(elemLength, elemLength + 20)
                .then(result => this.setState({
                    isInfiniteLoading: false,
                    content: this.state.content.concat(result)
                }));
        }, 500);
    };

    elementInfiniteLoad() {
        return <div>

        </div>
    }

    render() {
        return (
            <Infinite elementHeight={70}
                      useWindowAsScrollContainer
                      infiniteLoadBeginEdgeOffset={200}
                      onInfiniteLoad={this.handleInfiniteLoad}
                      loadingSpinnerDelegate={this.elementInfiniteLoad()}
                      isInfiniteLoading={this.state.isInfiniteLoading}
            >
                {this.state.content}
            </Infinite>
        );
    }
}

export default TopList