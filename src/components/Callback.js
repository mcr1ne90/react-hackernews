import React, { Component } from 'react';

class Callback extends Component {
    render() {
        return (
            <div>
                You're now redirecting...
            </div>
        );
    }
}

export default Callback;