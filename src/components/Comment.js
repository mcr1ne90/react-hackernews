import React from 'react';

function Comment(props) {
    return (
        <div className="Comment-body" dangerouslySetInnerHTML={{__html: props.text}}/>
    );
}

export default Comment