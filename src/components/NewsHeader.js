import React from 'react';
import {NavLink} from 'react-router-dom';

export default class NewsHeader extends React.Component {
    render() {
        return (
            <ul className="App-header">
                <li><NavLink to="/news" activeStyle={{
                    fontWeight: 'bold',
                    color: 'red'
                }}>
                    News
                </NavLink></li>
                <li><NavLink to="/top" activeStyle={{
                    fontWeight: 'bold',
                    color: 'red'
                }}>Top news</NavLink></li>
                <li><NavLink to="/jobs" activeStyle={{
                    fontWeight: 'bold',
                    color: 'red'
                }}>Jobs</NavLink></li>
                <li><NavLink to="/ask" activeStyle={{
                    fontWeight: 'bold',
                    color: 'red'
                }}>Ask</NavLink></li>
                <li><NavLink to="/show" activeStyle={{
                    fontWeight: 'bold',
                    color: 'red'
                }}>Show</NavLink></li>
                <li><NavLink to="/login" activeStyle={{
                    fontWeight: 'bold',
                    color: 'red'
                }}>Login</NavLink></li>
            </ul>
        );
    }
}