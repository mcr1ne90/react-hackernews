import React from 'react';
import axios from 'axios';
import P from 'bluebird';
import Comment from './Comment';
import CommentsLoader from './CommentsLoader';

class CommentsList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {isVisible: false, content: []};
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState({isVisible: !this.state.isVisible});
    }

    componentDidMount() {
        if (this.state.content.length === 0) {
            const ids = this.props.data;
            let comments = [];
            let commentsInfo = [];
            if (ids) {
                for (let item of ids) {
                    if (item.length !== 1) {
                        comments.push(axios.get('https://hacker-news.firebaseio.com/v0/item/' + item + '.json'));
                    }
                }
                return P.all(comments)
                    .then(comments => {
                            for (let el of comments) {
                                if (el.data.text)
                                    commentsInfo.push(el.data.text);
                            }
                            this.setState({content: commentsInfo});
                        }
                    );
            }
        }
    }

    render() {
        const entries = this.state.content;
        const comments = entries.map(entry =>
            <Comment key={entries.indexOf(entry).toString()} text={entry}/>
        );

        return (
            <div>
                <button testlink={true} className="Comments-button" onClick={this.handleClick}>show comments
                </button>
                {this.state.isVisible && (this.props.data.length > 5 ? <CommentsLoader content={comments}/> : comments)}
            </div>
        );

    }
}

export default CommentsList