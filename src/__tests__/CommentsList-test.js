import React from 'react';
import CommentsList from '../components/CommentsList';
import Enzyme, { shallow, mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

it('renders correctly', () => {
    const wrapper = shallow(<CommentsList data={['foo', 'bar', 'baz']}/>);
    expect(wrapper).toMatchSnapshot();
});

it('shows and hides comments', () => {
    const wrapper = shallow(<CommentsList data={['foo', 'bar', 'baz']}/>);

    //show comments
    wrapper.find({ testlink: true }).simulate('click');
    expect(wrapper.state().isVisible).toEqual(true);

    //hide comments
    wrapper.find({ testlink: true }).simulate('click');
    expect(wrapper.state().isVisible).toEqual(false);


});
