import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import { assert } from 'chai';
import Adapter from 'enzyme-adapter-react-16';

Enzyme.configure({ adapter: new Adapter() });

import Comment from '../components/Comment';

describe('Comment', () => {
    const LOADING_PROPS = {
        text: 'It\'s a comment'
    };
    const wrapper = shallow(<Comment {...LOADING_PROPS} />);

    it('should be wrapped in a div', () => {
        const commentContainer = wrapper.find('div');

        assert.equal(commentContainer.length, 1);
        assert.ok(commentContainer.hasClass('Comment-body'));
    });

    it('should have a comment with the correct text', () => {
        const loadingComment = wrapper.find('div');

        assert.equal(loadingComment.length, 1);
        assert.equal(loadingComment.prop('dangerouslySetInnerHTML').__html, LOADING_PROPS.text);
    });
});