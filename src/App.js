import React from 'react';
import NewsHeader from './components/NewsHeader';
import NewsList from './components/NewsList';
import JobsList from './components/JobsList';
import TopList from './components/TopList';
import AskList from './components/AskList';
import ShowList from './components/ShowList';
import LoginComponent from './components/LoginComponent';
import {
    Redirect,
    BrowserRouter as Router,
    Route,
} from 'react-router-dom';
import Callback from './components/Callback';
import Auth from './services/auth';
import Profile from './components/Profile';

const auth = new Auth();

const handleAuthentication = (nextState, replace) => {
    if (/access_token|id_token|error/.test(nextState.location.hash)) {
        auth.handleAuthentication();
    }
};

class App extends React.Component {
    render() {
        return (
            <Router>
                <div>
                    <NewsHeader/>
                    <Route exact={true} path="/news" component={NewsList}/>
                    <Route exact={true} path="/top" component={TopList}/>
                    <Route exact={true} path="/jobs" component={JobsList}/>
                    <Route exact={true} path="/ask" component={AskList}/>
                    <Route exact={true} path="/show" component={ShowList}/>
                    <Route exact={true} path="/login" render={(props) => <LoginComponent auth={auth} {...props} />}/>
                    <Route exact={true} path="/profile" render={(props) => (
                        !auth.isAuthenticated() ? (
                            <Redirect to="/news"/>
                        ) : (
                            <Profile auth={auth} {...props} />
                        )
                    )} />
                    <Route exact={true} path="/callback" render={(props) => {
                        handleAuthentication(props);
                        return <Callback {...props} />
                    }}/>
                </div>
            </Router>
        );
    }
}

export default App